package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface implémenté par ImageDeserializerBase64Stremainglmpl
 * @param <M>
 */
public interface ImageStreamingDeserializer<M> {

    /**
     *
     * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     *
     * @param media
     * @return
     * @param <K>
     * @throws IOException
     */
    <K extends InputStream> K getDeserializingStream(M media) throws IOException;

    /**
     *
     * @return
     * @param <T>
     */
    <T extends OutputStream> T getSourceOutputStream();
}
