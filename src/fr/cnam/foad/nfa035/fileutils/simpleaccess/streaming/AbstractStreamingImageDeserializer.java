package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

/**
 * Cette classe implémente la classe ImageStreamingDeserializer
 * @param <M>
 */
public abstract class AbstractStreamingImageDeserializer<M> implements ImageStreamingDeserializer<M> {
    /**
     * On deserialise le fichier media et on transfert le resultat dans une autre variable
     * @param media
     * @throws IOException
     */
    @Override
    public final void deserialize(M media) throws IOException {
        getDeserializingStream(media).transferTo(getSourceOutputStream());
    }
}