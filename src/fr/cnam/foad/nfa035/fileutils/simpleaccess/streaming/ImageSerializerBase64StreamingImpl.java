package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.util.Base64;

/**
 * Cette classe permet de sérialisé en base 64
 */
public class ImageSerializerBase64StreamingImpl implements ImageStreamingSerializer {

    /**
     *Dans cette méthode on récupère le stream de media en base 64
     * @param media
     * @return OutputStream
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(Object media) throws IOException {
        return new Base64OutputStream((OutputStream) media);
    }

    /**
     *Dans cette méthode on récupère le stream de source en base 64
     * @param source
     * @return InputStream
     * @throws IOException
     */
    @Override
    public InputStream getSourceInputStream(Object source) throws IOException {
        return new Base64InputStream((InputStream) source);
    }

    /**
     *
     * @param source
     * @param media
     * @throws IOException
     */
    @Override
    public void serialize(Object source, Object media) throws IOException {

    }
}
