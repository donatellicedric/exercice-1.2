package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface implémenté par ImageSerializerBase64Stremainglmpl
 * @param <S>
 * @param <M>
 */
public interface ImageStreamingSerializer<S, M> {

    /**
     *
     * @param source
     * @param media
     * @throws IOException
     */
    void serialize(S source, M media) throws IOException;

    /**
     *
     * @param source
     * @return
     * @param <K>
     * @throws IOException
     */
    <K extends InputStream> K getSourceInputStream(S source) throws IOException;

    /**
     *
     * @param media
     * @return
     * @param <T>
     * @throws IOException
     */
    <T extends OutputStream> T getSerializingStream(M media) throws IOException;
}
