package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;

/**
 * Cette classe créer le type de variable ImageByteArrayFrame
 * Elle dispose de 2 méthodes qui permettent d'obtenir l'encodage
 */
public class ImageByteArrayFrame extends AbstractImageFrameMedia{

        ByteArrayOutputStream outputStream;

    /**
     * Constructeur
     * @param output
     */
    public ImageByteArrayFrame(ByteArrayOutputStream output) {
        this.outputStream = output;
    }

    /**
     * renvoit l'encodage
     * @return outputStream
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {

        return outputStream;
    }

    /**
     * N'a pas été modifié car n'est pas utilisé dans le test
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {

        return null;
    }
}
