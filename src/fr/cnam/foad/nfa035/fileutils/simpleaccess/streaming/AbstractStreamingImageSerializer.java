package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.IOException;

/**
 * Cette classe implement ImageStreamingSerializer
 * @param <S>
 * @param <M>
 */
public abstract class AbstractStreamingImageSerializer<S,M> implements ImageStreamingSerializer<S,M> {
  /**
   * On serialise l'objet source puis ont transfert le resultat de la serialisation vers media
   * @param source
   * @param media
   * @throws IOException
   */
  @Override
  public final void serialize(S source, M media) throws IOException {
    getSourceInputStream(source).transferTo(getSerializingStream(media));
  }
}