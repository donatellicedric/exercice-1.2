package fr.cnam.foad.nfa035.fileutils.simpleaccess.streaming;

import java.io.*;

/**
 * Cette classe permet de déserialiser
 * Elle implémente la classe ImageStreamingDeserializer
 */
public class ImageDeserializerBase64StreamingImpl implements ImageStreamingDeserializer
{
    ByteArrayOutputStream deserialization;

    public ImageDeserializerBase64StreamingImpl(ByteArrayOutputStream deserializationOutput) {
        this.deserialization = deserializationOutput;
    }

    @Override
    public void deserialize(Object media) throws IOException {

    }

    /**
     * Cette méthode permet de récupéré le code désérialisé
     * @return le stream deserialisé
     */
    @Override
    public OutputStream getSourceOutputStream() {

        return deserialization;
    }

    @Override
    public InputStream getDeserializingStream(Object media) throws IOException {


        return null;
    }
}
